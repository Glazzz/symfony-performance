<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Service\CategoryService;
use AppBundle\Service\ProductService;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @var CategoryService
     */
    protected $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;

        $this->categoryService = $categoryService;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/insert-products", name="insert_products")
     */
    public function insertProductsAction(Request $request)
    {
        $this->productService->insertPerformance();
    }

    /**
     * @Route("/find-product-by-name", name="find_product_by_name")
     */
    public function findByNameAction(Request $request)
    {
        $this->productService->findByNamePerformance($request->get('name', 'product-2_4'));
    }

    /**
     * @Route("/find-products-by-category", name="find_products_by_category_id")
     */
    public function findProductsByCategoryIdAction(Request $request)
    {
        $this->productService->findByCategoryIdPerformance($request->get('category_id', 1));
    }

    /**
     * @Route("/find-products-by-text", name="find_products_by_text")
     */
    public function findProductsByTextAction(Request $request)
    {
        $this->productService->findByLikeTextPerformance($request->get('text', 'Product Description'));
    }

    /**
     * @Route("/single-find-by-name", name="single_find_by_name")
     */
    public function singleFindByNameAction()
    {
        return $this->json($this->productService->findByName());
    }

    /**
     * @Route("/single-insert-products", name="single_insert_products")
     */
    public function singleInsertProductsAction()
    {
        return $this->json($this->productService->insertProducts());
    }

    /**
     * @Route("/single-find-by-like-text", name="single_find_by_like_text")
     */
    public function singleFindByLikeTextAction()
    {
        return $this->json($this->productService->findByLikeText());
    }
}
