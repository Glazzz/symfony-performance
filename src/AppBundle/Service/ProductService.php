<?php

namespace AppBundle\Service;

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;

class ProductService
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var EntityManager
     */
    private $manager;

    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;

        $this->manager = $this->doctrine->getManager();
    }

    public function deleteAllProducts()
    {
        $products = $this->manager->getRepository(Product::class)->findAll();

        foreach ($products as $product) {
            $this->manager->remove($product);
        }

        $this->manager->flush();
    }

    private function showInfo($finish, $counter = null)
    {
        printf('<p>Time spent: %s</p>', $finish);
        printf('<p>Memory used: %s megabytes</p>', round(memory_get_usage(true) / 1048576,2));

        if ($counter) {
            printf('<p>%d rows.</p>', $counter);
            printf('<p>Requests per seconds: %s</p>', $counter / $finish);
        }

        die;
    }

    public function insertPerformance()
    {
        $this->deleteAllProducts();

        $categories = $this->manager->getRepository(Category::class)->findAll();

        $counter = 0;

        $start = microtime(true);

        foreach (range(0, 100) as $key => $item) {
            /** @var Category $category */
            foreach ($categories as $catKey => $category) {
                $product = new Product();

                $product->setName(sprintf('product-%s_%s', $key, $catKey))
                    ->setCategory($category)
                    ->setDescription(
                        sprintf(
                            'Product Description + category description %s',
                            $category->getDescription()
                        )
                    );

                // Save each item
                $this->manager->persist($product);
                $this->manager->flush();

                $counter++;
            }
        }

        $finish = microtime(true) - $start;

        $this->showInfo($finish, $counter);
    }

    public function findByNamePerformance($productName = 'product-2_4')
    {
        $repository = $this->manager->getRepository(Product::class);

        $start = microtime(true);

        foreach (range(0, 1000) as $item) {
            $repository->findBy(['name' => $productName]);
        }

        $finish = microtime(true) - $start;

        $this->showInfo($finish, 1000);
    }

    public function findByCategoryIdPerformance($categoryId = 1)
    {
        $qb = $this->manager->getRepository(Product::class)->createQueryBuilder('p');

        $start = microtime(true);

        foreach (range(0, 1000) as $item) {
            $result = $qb
                ->select('p')
                ->where('p.category = :categoryId')
                ->setParameter('categoryId', $categoryId)
                ->getQuery()
                ->getResult();
        }

        $finish = microtime(true) - $start;

        $this->showInfo($finish, 1000);
    }

    public function findByLikeTextPerformance($text = 'Product Description')
    {
        $qb = $this->manager->getRepository(Product::class)->createQueryBuilder('p');

        $start = microtime(true);

        foreach (range(0, 1000) as $item) {
            $result = $qb
                ->select('p')
                ->where('p.description LIKE :text')
                ->setParameter('text', '%s' . $text . '%s')
                ->getQuery()
                ->getResult();
        }

        $finish = microtime(true) - $start;

        $this->showInfo($finish, 1000);
    }

    public function findByName($productName = 'product-2_4')
    {
        $repository = $this->manager->getRepository(Product::class);

        return $repository->findBy(['name' => $productName]);;
    }

    public function insertProducts()
    {
        $categories = $this->manager->getRepository(Category::class)->findAll();

        $products = [];

        /** @var Category $category */
        foreach ($categories as $catKey => $category) {
            $product = new Product();

            $product->setName(sprintf('product-%s_%s', 'B', $catKey))
                ->setCategory($category)
                ->setDescription(
                    sprintf(
                        'Product Description + category description %s',
                        $category->getDescription()
                    )
                );

            // Save each item
            $this->manager->persist($product);
            $this->manager->flush();

            $products[] = $product;
        }

        return $products;
    }

    public function findByLikeText($text = 'Product Description')
    {
        $qb = $this->manager->getRepository(Product::class)->createQueryBuilder('p');

        return $qb
            ->select('p')
            ->where('p.description LIKE :text')
            ->setParameter('text', '%s' . $text . '%s')
            ->getQuery()
            ->getResult();
    }
}
